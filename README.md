# P4 implementation of BPP

Authors:

Vivien Maintenant, Telecom Nancy, France

Sébastien Tabor, Telecom Nancy, France

Jérôme François, Inria, France (contact: jerome.francois@inria.fr)

# Reference paper

Richard Li, Alexander Clemm, Uma Chunduri, Lijun Dong, and Kiran Makhijani. 2018. A New Framework and Protocol for Future Networking Applications. In Proceedings of the 2018 Workshop on Networking for Emerging Applications and Technologies (NEAT ’18). Association for Computing Machinery, New York, NY, USA, 21–26. DOI:https://doi.org/10.1145/3229574.3229576

## Pre-requisites

In order to run the application, p4app must first be installed using the following repository : [https://github.com/ImForgi/p4app](https://github.com/ImForgi/p4app)

## Usage

To run the application, being at the root of the repository use the following command :
```bash
p4app run .
```

You will then be rooted to a mininet command-line. (Mininet will also give information about the host's IP addresses.)

You can also use `p4app exec m h1 bash` inside another SSH connection to open a bash command-line on the host h1 (for example), and access all the h1 environment.

## Sources

The sources of the P4 application can be found inside `src/`.

## Settings

Settings of the P4 application can be found inside `p4app.json`.

## Testing scripts

Scripts for testing purpose can be found inside `scripts/`.

Since all files at the root of the P4 application are copied inside the `/tmp` folder of each host of the topology, you can access this scripts inside `/tmp/scripts` being in a host environment.

## Publication

If you like this project and use it, please acknowledge our publication:

Jérôme François, Alexander Clemm, Vivien Maintenant, Sébastien Tabor. BPP over P4: Exploring Frontiers and Limits in Programmable Packet Processing. IEEE Global Communications Conference, Dec 2020, Taipei, Taiwan
https://hal.archives-ouvertes.fr/hal-03032566

import datetime
from bpp import *
from scapy.all import sniff
import time

i = 0

start = time.time()


def queues(metadata):
    q = []

    for k in range(min(metadata.id, 10)):
        q.append(str(getattr(metadata, "data" + str(k + 1))))

    return ', '.join(q)


def parse(packet):
    global i
    i += 1
    print(str(time.time() - start) + ","+str(queues(packet[BPPMetadata])))
    #print("[" + str(datetime.datetime.now()) + "] BPP packet " + str(i) + ": queue depths = [" + queues(packet[BPPMetadata]) + "].")


def main():
    sniff(filter="ether proto 0x0800 and ip proto 0xfd", count=0, prn=parse)


if __name__ == '__main__':
    main()

from scapy.layers.inet import Packet, BitField
from scapy.layers.inet import Ether, IP, ICMP
from scapy.packet import bind_layers

# Next headers
IP_BPP = 0xfd
BPP_ICMP = 0x01
BPP_COMMAND = 0xf0
BPP_METADATA = 0xff

# BPP Parameter Categories
BPP_PARAMETER_CATEGORY_METADATA = 0x0001
BPP_PARAMETER_CATEGORY_STATELET = 0x0002
BPP_PARAMETER_CATEGORY_BUILTIN = 0x0003

# BPP Actions
BPP_ACTION_SUM = 0x01
BPP_ACTION_PUT = 0x02
BPP_ACTION_DROP = 0xff

# BPP Condition Operators
BPP_CONDITION_AND = 0x0001
BPP_CONDITION_OR = 0x0002
BPP_CONDITION_NOT = 0x01
BPP_CONDITION_EQUAL = 0x01
BPP_CONDITION_INFERIOR = 0x02
BPP_CONDITION_SUPERIOR = 0x03


class BPPHeader(Packet):
    name = "BPPHeader"
    fields_desc = [
        BitField("version", 0, 4),
        BitField("length", 0, 16),
        BitField("errorAction", 0, 4),
        BitField("priorErrors", 0, 2),
        BitField("v", 0, 2),
        BitField("tFlag", 0, 2),
        BitField("rsrvd", 0, 2),
        BitField("metadataOffset", 0, 8),
        BitField("checksum", 0, 16),
        BitField("next", 0, 8)
    ]


class BPPCommand(Packet):
    name = "BPPPCommand"
    fields_desc = [
        # Command header
        BitField("length", 0, 16),
        BitField("serialized", 0, 8),

        # Condition set
        BitField("conditionLength", 0, 16),
        BitField("conditionType", 0, 16),

        # Condition 1
        BitField("c1Length", 0, 8),
        BitField("c1Negation", 0, 8),
        BitField("c1Flags", 0, 8),
        BitField("c1Type", 0, 8),

        # Condition 1 Param 1
        BitField("c1p1Category", 0, 16),
        BitField("c1p1Length", 0, 16),
        BitField("c1p1Value", 0, 32),

        # Condition 1 Param 2
        BitField("c1p2Category", 0, 16),
        BitField("c1p2Length", 0, 16),
        BitField("c1p2Value", 0, 32),

        # Condition 2
        BitField("c2Length", 0, 8),
        BitField("c2Negation", 0, 8),
        BitField("c2Flags", 0, 8),
        BitField("c2Type", 0, 8),

        # Condition 2 Param 1
        BitField("c2p1Category", 0, 16),
        BitField("c2p1Length", 0, 16),
        BitField("c2p1Value", 0, 32),

        # Condition 2 Param 2
        BitField("c2p2Category", 0, 16),
        BitField("c2p2Length", 0, 16),
        BitField("c2p2Value", 0, 32),

        # Action set
        BitField("actionLength", 0, 32),

        # Action 1
        BitField("a1Length", 0, 8),
        BitField("a1Serialized", 0, 8),
        BitField("a1Flags", 0, 8),
        BitField("a1Type", 0, 8),

        # Action 1 Param 1
        BitField("a1p1Category", 0, 16),
        BitField("a1p1Length", 0, 16),
        BitField("a1p1Value", 0, 32),

        # Action 1 Param 2
        BitField("a1p2Category", 0, 16),
        BitField("a1p2Length", 0, 16),
        BitField("a1p2Value", 0, 32),

        # Action 2
        BitField("a2Length", 0, 8),
        BitField("a2Serialized", 0, 8),
        BitField("a2Flags", 0, 8),
        BitField("a2Type", 0, 8),

        # Action 2 Param 1
        BitField("a2p1Category", 0, 16),
        BitField("a2p1Length", 0, 16),
        BitField("a2p1Value", 0, 32),

        # Action 2 Param 2
        BitField("a2p2Category", 0, 16),
        BitField("a2p2Length", 0, 16),
        BitField("a2p2Value", 0, 32),

        # Next header
        BitField("next", 0, 8)

    ]


class BPPMetadata(Packet):
    name = "BPPMetadata"
    fields_desc = [
        BitField("id", 0, 64),
        BitField("data1", 0, 64),
        BitField("data2", 0, 64),
        BitField("data3", 0, 64),
        BitField("data4", 0, 64),
        BitField("data5", 0, 64),
        BitField("data6", 0, 64),
        BitField("data7", 0, 64),
        BitField("data8", 0, 64),
        BitField("data9", 0, 64),
        BitField("data10", 0, 64),
        BitField("empty", 0, 24),
        BitField("next", 0, 8)
    ]


bind_layers(IP, BPPHeader, proto=IP_BPP)
bind_layers(BPPHeader, BPPCommand, next=BPP_COMMAND)
bind_layers(BPPCommand, BPPCommand, next=BPP_COMMAND)
bind_layers(BPPCommand, BPPMetadata, next=BPP_METADATA)
bind_layers(BPPCommand, ICMP, next=BPP_ICMP)
bind_layers(BPPMetadata, ICMP, next=BPP_ICMP)




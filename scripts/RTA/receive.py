import datetime
from bpp import *
from scapy.all import sniff
import time

i = 0

#convert to second current time
start = time.time()

def parse(packet):
    global i
    i += 1
    #print("[" + str(datetime.datetime.now()) + "] Received real-time BPP packet " + str(i) + ".")

    print(str(time.time() - start) + ","+str(packet[IP].id))

def main():
    sniff(filter="ether proto 0x0800 and ip proto 0xfd", count=0, prn=parse)


if __name__ == '__main__':
    main()

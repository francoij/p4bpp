import sys
import time
from bpp import *
from scapy.all import sendp

def main():
    if len(sys.argv) < 2 :
        print("Usage: python send.py <IP> [packet amount].")
    else:
        nbpkts = 1
        wait = 1

        if len(sys.argv) > 2:
            try:
                nbpkts = int(sys.argv[2])
            except ValueError:
                print("Could not parse argument into int.")

        if len(sys.argv) > 3:
            try:
                wait = int(sys.argv[3])
            except ValueError:
                print("Could not parse argument into int.")

        packet = Ether(type=0x0800) / IP(dst=sys.argv[1], proto=IP_BPP) / BPPHeader(next=BPP_COMMAND) / \
                 BPPCommand(
                     length=0x0000,
                     serialized=0x00,

                     actionLength=0x00000001,

                     a1Length=0x02,
                     a1Type=BPP_ACTION_SUM,

                     a1p1Category=BPP_PARAMETER_CATEGORY_BUILTIN,
                     a1p1Value=0x01,

                     a1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     a1p2Value=0x01,

                     next=BPP_COMMAND) / \
                 BPPCommand(
                     length=0x0000,
                     serialized=0x00,

                     actionLength=0x00000001,

                     a1Length=0x02,
                     a1Type=BPP_ACTION_SUM,

                     a1p1Category=BPP_PARAMETER_CATEGORY_BUILTIN,
                     a1p1Value=0x02,

                     a1p2Category=BPP_PARAMETER_CATEGORY_METADATA,
                     a1p2Value=0x02,

                     next=BPP_METADATA) / \
                 BPPMetadata(next=BPP_ICMP) / ICMP()

        packet.show()


        for j in range(nbpkts):
            print("Sending packet " + str(j) + "...")
            sendp(packet)
            packet[IP].id += 1
            time.sleep(wait)

        print(str(nbpkts) + " packets sent.")



if __name__ == '__main__':
    main()

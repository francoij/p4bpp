import sys
from scapy.layers.inet import Ether, IP, ICMP
from scapy.all import sniff, sendp
from bpp import *


def parse(packet):
    print("Received 1 packets.")

    save = packet[Ether].src
    packet[Ether].src = packet[Ether].dst
    packet[Ether].dst = save

    save = packet[IP].src
    packet[IP].src = packet[IP].dst
    packet[IP].dst = save

    sendp(packet)


def main():
    if len(sys.argv) < 2:
        print("Usage: python echo.py <IP>.")
    else:
        sniff(filter="ether proto 0x0800 and ip proto 0xfd and ip dst " + sys.argv[1], count=0, prn=parse)


if __name__ == '__main__':
    main()

#Packet sniffer in python for Linux
#Sniffs only incoming TCP packet

import socket, sys
from struct import *

import datetime
from bpp import *
import time
import sys
import signal

i = 0
time_last = -1
resolution = 100
nlogs = 0
prefix_log = sys.argv[1]

tot_pkt = 0
start_time = -1
last_time = 0
tot_bytes = 0

class TimeoutException(Exception): pass

def parse_silent(packet):

	ip_header = packet[14:34]

	iph = unpack('!BBHHHBBH4s4s' , ip_header)

	version_ihl = iph[0]
	version = version_ihl >> 4
	ihl = version_ihl & 0xF

	iph_length = ihl * 4

	ttl = iph[5]
	protocol = iph[6]
	s_addr = socket.inet_ntoa(iph[8]);
	d_addr = socket.inet_ntoa(iph[9]);



	if protocol == 0xfd:
		global tot_pkt
		global start_time
		global last_time
		global tot_bytes
		current = time.time()

		if start_time < 0:
			start_time = current

		last_time=current
		tot_pkt+=1
		tot_bytes+=len(packet)

def parse(packet):

	ip_header = packet[14:34]

	iph = unpack('!BBHHHBBH4s4s' , ip_header)

	version_ihl = iph[0]
	version = version_ihl >> 4
	ihl = version_ihl & 0xF

	iph_length = ihl * 4

	ttl = iph[5]
	protocol = iph[6]
	s_addr = socket.inet_ntoa(iph[8]);
	d_addr = socket.inet_ntoa(iph[9]);

	if protocol == 0xfd:
#		print 'Version : ' + str(version) + ' IP Header Length : ' + str(ihl) + ' TTL : ' + str(ttl) + ' Protocol : ' + str(protocol) + ' Source Address : ' + str(s_addr) + ' Destination Address : ' + str(d_addr)

		global i
		global time_last
		current = time.time()
		global nlogs

		if time_last < 0:
			time_last = current
		elif i%resolution == 0:
			#print("Througput(pkt/sec):"+str(1.0*resolution/(current-time_last)))
			print(prefix_log+","+str(1.0*resolution/(current-time_last)))
			nlogs+=1
			time_last = current

		if nlogs>9:
			sys.exit()
		i += 1


def signal_handler(signum, frame):
	raise TimeoutException("Timed out!")


def main():
	try:
		s = socket.socket(socket.PF_PACKET, socket.SOCK_RAW, socket.ntohs(0x0800))

	except socket.error:
		print 'Socket error'
		sys.exit()

	signal.signal(signal.SIGALRM, signal_handler)
	signal.alarm(20)

	try:
		while True:
			packet = s.recvfrom(65565)
			packet = packet[0]
			parse_silent(packet)
	except TimeoutException as e:
		global tot_pkt
		global start_time
		global last_time
		print prefix_log+","+str(tot_pkt)+","+str(1.0*tot_pkt/(last_time-start_time))+","+str(tot_bytes)+","+str(1.0*tot_bytes/(1024**2*(last_time-start_time)))

if __name__ == '__main__':
		main()

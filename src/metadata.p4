#ifndef _METADATA_P4_
#define _METADATA_P4_

struct ingress_metadata_t {
    bit<32> nhop_ipv4;
    bit<32> nhop_ipv6;
}

struct metadata {
    @name("ingress_metadata")
    ingress_metadata_t   ingress_metadata;
}

#endif
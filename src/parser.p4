#ifndef _PARSER_P4_
#define _PARSER_P4_

#include "headers/headers.p4"
#include "metadata.p4"

parser BPPParser(packet_in packet, out headers hdr, inout metadata data, inout standard_metadata_t standard_metadata) {
    state start {
        transition parse_ethernet;
    }

    state parse_ethernet {
        packet.extract(hdr.ethernet);
        transition select(hdr.ethernet.etherType) {
            TYPE_IPV4: parse_ipv4;
            TYPE_IPV6: parse_ipv6;
            default: accept;
        }
    }

    state parse_ipv4 {
        packet.extract(hdr.ip.ipv4);
        transition select(hdr.ip.ipv4.protocol) {
            IPV4_BPP_PROTOCOL: parse_bpp;
            default: accept;
        }
    }

    state parse_ipv6 {
        packet.extract(hdr.ip.ipv6);
        transition select(hdr.ip.ipv6.protocol) {
            IPV6_BPP_HEADER: parse_bpp;
            default: accept;
        }
    }

    state parse_bpp {
        packet.extract(hdr.bpp.hdr);
        transition select(hdr.bpp.hdr.next) {
            BPP_COMMAND: parse_bpp_cmd;
            default: accept;
        }
    }

    state parse_bpp_cmd {
        packet.extract(hdr.bpp.cmd.next);
        transition select(hdr.bpp.cmd.last.next) {
            BPP_COMMAND: parse_bpp_cmd;
            BPP_METADATA: parse_bpp_metadata;
            default: accept;
        }
    }

    state parse_bpp_metadata {
        packet.extract(hdr.bpp_md);
        transition accept;
    }
}

#endif

#ifndef _TRAFFIC_P4_
#define _TRAFFIC_P4_

#include "headers/headers.p4"
#include "metadata.p4"

#define IPV4_LPM_TABLE_SIZE 1024
#define IPV4_FORWARD_TABLE_SIZE 512
#define IPV6_LPM_TABLE_SIZE 1024
#define SEND_FRAME_TABLE_SIZE 256

control BPPIngress(inout headers hdr, inout metadata meta, inout standard_metadata_t standard_metadata) {
    action set_nhop(ipv4Addr_t nhop_ipv4, bit<9> port) {
        meta.ingress_metadata.nhop_ipv4 = nhop_ipv4;
        standard_metadata.egress_spec = port;
        hdr.ip.ipv4.ttl = hdr.ip.ipv4.ttl + 8w255;
    }

    action set_dmac(macAddr_t dmac) {
        hdr.ethernet.dstAddr = dmac;
    }

    action _drop() {
        mark_to_drop(standard_metadata);
    }

    table ipv4_lpm {
        key = {
            hdr.ip.ipv4.dstAddr: lpm;
        }

        actions = {
            _drop;
            set_nhop;
            NoAction;
        }

        size = IPV4_LPM_TABLE_SIZE;
        default_action = NoAction();
    }

    table forward {
        key = {
            meta.ingress_metadata.nhop_ipv4: exact;
        }

        actions = {
            set_dmac;
            _drop;
            NoAction;
        }

        size = IPV4_FORWARD_TABLE_SIZE;
        default_action = NoAction();
    }

    action increment_bpp_id() {
        hdr.bpp_md.id = hdr.bpp_md.id + 1;
    }

    apply {
        if (hdr.ip.ipv4.isValid()) {
            ipv4_lpm.apply();
            forward.apply();
        }

        if (hdr.bpp.hdr.isValid() && hdr.bpp_md.isValid()) {
            increment_bpp_id();
        }
    }
}

control BPPEgress(inout headers hdr, inout metadata meta, inout standard_metadata_t standard_metadata) {
    action rewrite_mac(macAddr_t smac) {
        hdr.ethernet.srcAddr = smac;
    }

    action _drop() {
        mark_to_drop(standard_metadata);
    }

    table send_frame {
        key = {
            standard_metadata.egress_port: exact;
        }

        actions = {
            rewrite_mac;
            _drop;
            NoAction;
        }

        size = SEND_FRAME_TABLE_SIZE;
        default_action = NoAction();
    }

    action compute_condition_set(inout bit<1> result, bit<16> c_type, bit<1> b1, bit<1> b2) {
        result = 0;
        if (c_type == BPP_CONDITION_AND && b1 == 1 && b2 == 1) result = 1;
        else if (c_type == BPP_CONDITION_OR && (b1 == 1 || b2 == 1)) result = 1;
    }

    action compute_condition(inout bit<1> result, bit<8> c_negation, bit<8> c_type, bit<16> p1_c, bit<32> p1_v, bit<16> p2_c, bit<32> p2_v) {
        bit<64> v1 = 0;
        bit<64> v2 = 0;

        result = 0;

        if (p1_c == BPP_PARAMETER_CATEGORY_BUILTIN) {
            if (p1_v == 0x00000001) v1 = (bit<64>)(standard_metadata.egress_global_timestamp - standard_metadata.ingress_global_timestamp);
            else if (p1_v == 0x00000002) v1 = (bit<64>)(standard_metadata.deq_timedelta);
            else if (p1_v == 0x00000003) v1 = (bit<64>)(standard_metadata.enq_qdepth);
            else if (p1_v == 0x00000004) v1 = (bit<64>)(standard_metadata.deq_qdepth);
        } else if (p1_c == BPP_PARAMETER_CATEGORY_METADATA) {
            if (p1_v == 0x00000000) v1 = hdr.bpp_md.id;
            else if (p1_v == 0x00000001) v1 = hdr.bpp_md.data1;
            else if (p1_v == 0x00000002) v1 = hdr.bpp_md.data2;
            else if (p1_v == 0x00000003) v1 = hdr.bpp_md.data3;
            else if (p1_v == 0x00000004) v1 = hdr.bpp_md.data4;
            else if (p1_v == 0x00000005) v1 = hdr.bpp_md.data5;
            else if (p1_v == 0x00000006) v1 = hdr.bpp_md.data6;
            else if (p1_v == 0x00000007) v1 = hdr.bpp_md.data7;
            else if (p1_v == 0x00000008) v1 = hdr.bpp_md.data8;
            else if (p1_v == 0x00000009) v1 = hdr.bpp_md.data9;
            else if (p1_v == 0x0000000a) v1 = hdr.bpp_md.data10;
        } else if (p1_c == BPP_PARAMETER_CATEGORY_STATELET) {
            v1 = (bit<64>)p1_v;
        }

        if (p2_c == BPP_PARAMETER_CATEGORY_BUILTIN) {
            if (p2_v == 0x00000001) v2 = (bit<64>)(standard_metadata.egress_global_timestamp - standard_metadata.ingress_global_timestamp);
            else if (p2_v == 0x00000002) v2 = (bit<64>)(standard_metadata.deq_timedelta);
            else if (p2_v == 0x00000003) v2 = (bit<64>)(standard_metadata.enq_qdepth);
            else if (p2_v == 0x00000004) v2 = (bit<64>)(standard_metadata.deq_qdepth);
        } else if (p2_c == BPP_PARAMETER_CATEGORY_METADATA) {
            if (p2_v == 0x00000000) v2 = hdr.bpp_md.id;
            else if (p2_v == 0x00000001) v2 = hdr.bpp_md.data1;
            else if (p2_v == 0x00000002) v2 = hdr.bpp_md.data2;
            else if (p2_v == 0x00000003) v2 = hdr.bpp_md.data3;
            else if (p2_v == 0x00000004) v2 = hdr.bpp_md.data4;
            else if (p2_v == 0x00000005) v2 = hdr.bpp_md.data5;
            else if (p2_v == 0x00000006) v2 = hdr.bpp_md.data6;
            else if (p2_v == 0x00000007) v2 = hdr.bpp_md.data7;
            else if (p2_v == 0x00000008) v2 = hdr.bpp_md.data8;
            else if (p2_v == 0x00000009) v2 = hdr.bpp_md.data9;
            else if (p2_v == 0x0000000a) v2 = hdr.bpp_md.data10;
        } else if (p2_c == BPP_PARAMETER_CATEGORY_STATELET) {
            v2 = (bit<64>)p2_v;
        }

        if (c_type == BPP_CONDITION_EQUAL && v1 == v2) result = 1;
        else if (c_type == BPP_CONDITION_INFERIOR && v1 < v2) result = 1;
        else if (c_type == BPP_CONDITION_SUPERIOR && v1 > v2) result = 1;

        if (c_negation == BPP_CONDITION_NOT) result = 1 - result;
    }

    action sum(bit<16> p1_c, bit<32> p1_v, bit<16> p2_c, bit<32> p2_v) {
        bit<64> value = 0;

        if (p1_c == BPP_PARAMETER_CATEGORY_BUILTIN) {
            if (p1_v == 0x00000001) value = (bit<64>)(standard_metadata.egress_global_timestamp - standard_metadata.ingress_global_timestamp);
            else if (p1_v == 0x00000002) value = (bit<64>)(standard_metadata.deq_timedelta);
            else if (p1_v == 0x00000003) value = (bit<64>)(standard_metadata.enq_qdepth);
            else if (p1_v == 0x00000004) value = (bit<64>)(standard_metadata.deq_qdepth);
        } else if (p1_c == BPP_PARAMETER_CATEGORY_METADATA) {
            if (p1_v == 0x00000000) value = hdr.bpp_md.id;
            else if (p1_v == 0x00000001) value = hdr.bpp_md.data1;
            else if (p1_v == 0x00000002) value = hdr.bpp_md.data2;
            else if (p1_v == 0x00000003) value = hdr.bpp_md.data3;
            else if (p1_v == 0x00000004) value = hdr.bpp_md.data4;
            else if (p1_v == 0x00000005) value = hdr.bpp_md.data5;
            else if (p1_v == 0x00000006) value = hdr.bpp_md.data6;
            else if (p1_v == 0x00000007) value = hdr.bpp_md.data7;
            else if (p1_v == 0x00000008) value = hdr.bpp_md.data8;
            else if (p1_v == 0x00000009) value = hdr.bpp_md.data9;
            else if (p1_v == 0x0000000a) value = hdr.bpp_md.data10;
        } else if (p1_c == BPP_PARAMETER_CATEGORY_STATELET) {
            value = (bit<64>)p1_v;
        }

        if (p2_c == BPP_PARAMETER_CATEGORY_METADATA) {
            if (p2_v == 0x00000000) hdr.bpp_md.id = hdr.bpp_md.id + value;
            else if (p2_v == 0x00000001) hdr.bpp_md.data1 = hdr.bpp_md.data1 + value;
            else if (p2_v == 0x00000002) hdr.bpp_md.data2 = hdr.bpp_md.data2 + value;
            else if (p2_v == 0x00000003) hdr.bpp_md.data3 = hdr.bpp_md.data3 + value;
            else if (p2_v == 0x00000004) hdr.bpp_md.data4 = hdr.bpp_md.data4 + value;
            else if (p2_v == 0x00000005) hdr.bpp_md.data5 = hdr.bpp_md.data5 + value;
            else if (p2_v == 0x00000006) hdr.bpp_md.data6 = hdr.bpp_md.data6 + value;
            else if (p2_v == 0x00000007) hdr.bpp_md.data7 = hdr.bpp_md.data7 + value;
            else if (p2_v == 0x00000008) hdr.bpp_md.data8 = hdr.bpp_md.data8 + value;
            else if (p2_v == 0x00000009) hdr.bpp_md.data9 = hdr.bpp_md.data9 + value;
            else if (p2_v == 0x0000000a) hdr.bpp_md.data10 = hdr.bpp_md.data10 + value;
        }
    }

    action put(bit<16> p1_c, bit<32> p1_v, bit<16> p2_c, bit<32> p2_v) {
        bit<64> value = 0;

        if (p1_c == BPP_PARAMETER_CATEGORY_BUILTIN) {
            if (p1_v == 0x00000001) value = (bit<64>)(standard_metadata.egress_global_timestamp - standard_metadata.ingress_global_timestamp);
            else if (p1_v == 0x00000002) value = (bit<64>)(standard_metadata.deq_timedelta);
            else if (p1_v == 0x00000003) value = (bit<64>)(standard_metadata.enq_qdepth);
            else if (p1_v == 0x00000004) value = (bit<64>)(standard_metadata.deq_qdepth);
        } else if (p1_c == BPP_PARAMETER_CATEGORY_METADATA) {
            if (p1_v == 0x00000000) value = hdr.bpp_md.id;
            else if (p1_v == 0x00000001) value = hdr.bpp_md.data1;
            else if (p1_v == 0x00000002) value = hdr.bpp_md.data2;
            else if (p1_v == 0x00000003) value = hdr.bpp_md.data3;
            else if (p1_v == 0x00000004) value = hdr.bpp_md.data4;
            else if (p1_v == 0x00000005) value = hdr.bpp_md.data5;
            else if (p1_v == 0x00000006) value = hdr.bpp_md.data6;
            else if (p1_v == 0x00000007) value = hdr.bpp_md.data7;
            else if (p1_v == 0x00000008) value = hdr.bpp_md.data8;
            else if (p1_v == 0x00000009) value = hdr.bpp_md.data9;
            else if (p1_v == 0x0000000a) value = hdr.bpp_md.data10;
        } else if (p1_c == BPP_PARAMETER_CATEGORY_STATELET) {
            value = (bit<64>)p1_v;
        }

        if (p2_c == BPP_PARAMETER_CATEGORY_METADATA) {
            if (p2_v == 0x00000000) hdr.bpp_md.id = value;
            else if (p2_v == 0x00000001) hdr.bpp_md.data1 = value;
            else if (p2_v == 0x00000002) hdr.bpp_md.data2 = value;
            else if (p2_v == 0x00000003) hdr.bpp_md.data3 = value;
            else if (p2_v == 0x00000004) hdr.bpp_md.data4 = value;
            else if (p2_v == 0x00000005) hdr.bpp_md.data5 = value;
            else if (p2_v == 0x00000006) hdr.bpp_md.data6 = value;
            else if (p2_v == 0x00000007) hdr.bpp_md.data7 = value;
            else if (p2_v == 0x00000008) hdr.bpp_md.data8 = value;
            else if (p2_v == 0x00000009) hdr.bpp_md.data9 = value;
            else if (p2_v == 0x0000000a) hdr.bpp_md.data10 = value;
        }
    }

    apply {
        if (hdr.ip.ipv4.isValid() || hdr.ip.ipv6.isValid()) {
          send_frame.apply();
        }

        if (hdr.bpp.hdr.isValid() && hdr.bpp_md.isValid()) {
            if (hdr.bpp.cmd.size > 0 && hdr.bpp.cmd[0].isValid()) {
                bit<1> c_result = 0;
                bit<1> c1_result = 0;
                bit<1> c2_result = 0;
                bit<16> conditionType = 1;

                if (hdr.bpp.cmd[0].conditionLength > 0) {
                    compute_condition(c1_result, hdr.bpp.cmd[0].c1Negation, hdr.bpp.cmd[0].c1Type, hdr.bpp.cmd[0].c1p1Category, hdr.bpp.cmd[0].c1p1Value, hdr.bpp.cmd[0].c1p2Category, hdr.bpp.cmd[0].c1p2Value);

                    if (hdr.bpp.cmd[0].conditionLength > 1) {
                        compute_condition(c2_result, hdr.bpp.cmd[0].c2Negation, hdr.bpp.cmd[0].c2Type, hdr.bpp.cmd[0].c2p1Category, hdr.bpp.cmd[0].c2p1Value, hdr.bpp.cmd[0].c2p2Category, hdr.bpp.cmd[0].c2p2Value);
                        conditionType = hdr.bpp.cmd[0].conditionType;
                    } else {
                        c2_result = 1;
                    }

                    compute_condition_set(c_result, conditionType, c1_result, c2_result);
                }

                if (hdr.bpp.cmd[0].conditionLength == 0 || c_result == 1) {
                    if (hdr.bpp.cmd[0].actionLength > 0) {
                        if (hdr.bpp.cmd[0].a1Type == BPP_ACTION_SUM && hdr.bpp.cmd[0].a1Length == 2)
                            sum(hdr.bpp.cmd[0].a1p1Category, hdr.bpp.cmd[0].a1p1Value, hdr.bpp.cmd[0].a1p2Category, hdr.bpp.cmd[0].a1p2Value);

                        else if (hdr.bpp.cmd[0].a1Type == BPP_ACTION_PUT && hdr.bpp.cmd[0].a1Length == 2)
                            put(hdr.bpp.cmd[0].a1p1Category, hdr.bpp.cmd[0].a1p1Value, hdr.bpp.cmd[0].a1p2Category, hdr.bpp.cmd[0].a1p2Value);

                        else if (hdr.bpp.cmd[0].a1Type == BPP_ACTION_DROP)
                            _drop();
                    }
                }
            }

            if (hdr.bpp.cmd.size > 1 && hdr.bpp.cmd[1].isValid()) {
                bit<1> c_result = 0;
                bit<1> c1_result = 0;
                bit<1> c2_result = 0;
                bit<16> conditionType = 1;

                if (hdr.bpp.cmd[1].conditionLength > 0) {
                    compute_condition(c1_result, hdr.bpp.cmd[1].c1Negation, hdr.bpp.cmd[1].c1Type, hdr.bpp.cmd[1].c1p1Category, hdr.bpp.cmd[1].c1p1Value, hdr.bpp.cmd[1].c1p2Category, hdr.bpp.cmd[1].c1p2Value);

                    if (hdr.bpp.cmd[1].conditionLength > 1) {
                        compute_condition(c2_result, hdr.bpp.cmd[1].c2Negation, hdr.bpp.cmd[1].c2Type, hdr.bpp.cmd[1].c2p1Category, hdr.bpp.cmd[1].c2p1Value, hdr.bpp.cmd[1].c2p2Category, hdr.bpp.cmd[1].c2p2Value);
                        conditionType = hdr.bpp.cmd[1].conditionType;
                    } else {
                        c2_result = 1;
                    }

                    compute_condition_set(c_result, conditionType, c1_result, c2_result);
                }

                if (hdr.bpp.cmd[1].conditionLength == 0 || c_result == 1) {
                    if (hdr.bpp.cmd[1].actionLength > 0) {
                        if (hdr.bpp.cmd[1].a1Type == BPP_ACTION_SUM && hdr.bpp.cmd[1].a1Length == 2)
                            sum(hdr.bpp.cmd[1].a1p1Category, hdr.bpp.cmd[1].a1p1Value, hdr.bpp.cmd[1].a1p2Category, hdr.bpp.cmd[1].a1p2Value);

                        else if (hdr.bpp.cmd[1].a1Type == BPP_ACTION_PUT && hdr.bpp.cmd[1].a1Length == 2)
                            put(hdr.bpp.cmd[1].a1p1Category, hdr.bpp.cmd[1].a1p1Value, hdr.bpp.cmd[1].a1p2Category, hdr.bpp.cmd[1].a1p2Value);

                        else if (hdr.bpp.cmd[1].a1Type == BPP_ACTION_DROP)
                            _drop();
                    }
                }
            }

            if (hdr.bpp.cmd.size > 2 && hdr.bpp.cmd[2].isValid()) {
                bit<1> c_result = 0;
                bit<1> c1_result = 0;
                bit<1> c2_result = 0;
                bit<16> conditionType = 1;

                if (hdr.bpp.cmd[2].conditionLength > 0) {
                    compute_condition(c1_result, hdr.bpp.cmd[2].c1Negation, hdr.bpp.cmd[2].c1Type, hdr.bpp.cmd[2].c1p1Category, hdr.bpp.cmd[2].c1p1Value, hdr.bpp.cmd[2].c1p2Category, hdr.bpp.cmd[2].c1p2Value);

                    if (hdr.bpp.cmd[2].conditionLength > 1) {
                        compute_condition(c2_result, hdr.bpp.cmd[2].c2Negation, hdr.bpp.cmd[2].c2Type, hdr.bpp.cmd[2].c2p1Category, hdr.bpp.cmd[2].c2p1Value, hdr.bpp.cmd[2].c2p2Category, hdr.bpp.cmd[2].c2p2Value);
                        conditionType = hdr.bpp.cmd[2].conditionType;
                    } else {
                        c2_result = 1;
                    }

                    compute_condition_set(c_result, conditionType, c1_result, c2_result);
                }

                if (hdr.bpp.cmd[2].conditionLength == 0 || c_result == 1) {
                    if (hdr.bpp.cmd[2].actionLength > 0) {
                        if (hdr.bpp.cmd[2].a1Type == BPP_ACTION_SUM && hdr.bpp.cmd[2].a1Length == 2)
                            sum(hdr.bpp.cmd[2].a1p1Category, hdr.bpp.cmd[2].a1p1Value, hdr.bpp.cmd[2].a1p2Category, hdr.bpp.cmd[2].a1p2Value);

                        else if (hdr.bpp.cmd[2].a1Type == BPP_ACTION_PUT && hdr.bpp.cmd[2].a1Length == 2)
                            put(hdr.bpp.cmd[2].a1p1Category, hdr.bpp.cmd[2].a1p1Value, hdr.bpp.cmd[2].a1p2Category, hdr.bpp.cmd[2].a1p2Value);

                        else if (hdr.bpp.cmd[2].a1Type == BPP_ACTION_DROP)
                            _drop();
                    }
                }
            }

            if (hdr.bpp.cmd.size > 3 && hdr.bpp.cmd[3].isValid()) {
                bit<1> c_result = 0;
                bit<1> c1_result = 0;
                bit<1> c2_result = 0;
                bit<16> conditionType = 1;

                if (hdr.bpp.cmd[3].conditionLength > 0) {
                    compute_condition(c1_result, hdr.bpp.cmd[3].c1Negation, hdr.bpp.cmd[3].c1Type, hdr.bpp.cmd[3].c1p1Category, hdr.bpp.cmd[3].c1p1Value, hdr.bpp.cmd[3].c1p2Category, hdr.bpp.cmd[3].c1p2Value);

                    if (hdr.bpp.cmd[3].conditionLength > 1) {
                        compute_condition(c2_result, hdr.bpp.cmd[3].c2Negation, hdr.bpp.cmd[3].c2Type, hdr.bpp.cmd[3].c2p1Category, hdr.bpp.cmd[3].c2p1Value, hdr.bpp.cmd[3].c2p2Category, hdr.bpp.cmd[3].c2p2Value);
                        conditionType = hdr.bpp.cmd[3].conditionType;
                    } else {
                        c2_result = 1;
                    }

                    compute_condition_set(c_result, conditionType, c1_result, c2_result);
                }

                if (hdr.bpp.cmd[3].conditionLength == 0 || c_result == 1) {
                    if (hdr.bpp.cmd[3].actionLength > 0) {
                        if (hdr.bpp.cmd[3].a1Type == BPP_ACTION_SUM && hdr.bpp.cmd[3].a1Length == 2)
                            sum(hdr.bpp.cmd[3].a1p1Category, hdr.bpp.cmd[3].a1p1Value, hdr.bpp.cmd[3].a1p2Category, hdr.bpp.cmd[3].a1p2Value);

                        else if (hdr.bpp.cmd[3].a1Type == BPP_ACTION_PUT && hdr.bpp.cmd[3].a1Length == 2)
                            put(hdr.bpp.cmd[3].a1p1Category, hdr.bpp.cmd[3].a1p1Value, hdr.bpp.cmd[3].a1p2Category, hdr.bpp.cmd[3].a1p2Value);

                        else if (hdr.bpp.cmd[3].a1Type == BPP_ACTION_DROP)
                            _drop();
                    }
                }
            }

            if (hdr.bpp.cmd.size > 4 && hdr.bpp.cmd[4].isValid()) {
                bit<1> c_result = 0;
                bit<1> c1_result = 0;
                bit<1> c2_result = 0;
                bit<16> conditionType = 1;

                if (hdr.bpp.cmd[4].conditionLength > 0) {
                    compute_condition(c1_result, hdr.bpp.cmd[4].c1Negation, hdr.bpp.cmd[4].c1Type, hdr.bpp.cmd[4].c1p1Category, hdr.bpp.cmd[4].c1p1Value, hdr.bpp.cmd[4].c1p2Category, hdr.bpp.cmd[4].c1p2Value);

                    if (hdr.bpp.cmd[4].conditionLength > 1) {
                        compute_condition(c2_result, hdr.bpp.cmd[4].c2Negation, hdr.bpp.cmd[4].c2Type, hdr.bpp.cmd[4].c2p1Category, hdr.bpp.cmd[4].c2p1Value, hdr.bpp.cmd[4].c2p2Category, hdr.bpp.cmd[4].c2p2Value);
                        conditionType = hdr.bpp.cmd[4].conditionType;
                    } else {
                        c2_result = 1;
                    }

                    compute_condition_set(c_result, conditionType, c1_result, c2_result);
                }

                if (hdr.bpp.cmd[4].conditionLength == 0 || c_result == 1) {
                    if (hdr.bpp.cmd[4].actionLength > 0) {
                        if (hdr.bpp.cmd[4].a1Type == BPP_ACTION_SUM && hdr.bpp.cmd[4].a1Length == 2)
                            sum(hdr.bpp.cmd[4].a1p1Category, hdr.bpp.cmd[4].a1p1Value, hdr.bpp.cmd[4].a1p2Category, hdr.bpp.cmd[4].a1p2Value);

                        else if (hdr.bpp.cmd[4].a1Type == BPP_ACTION_PUT && hdr.bpp.cmd[4].a1Length == 2)
                            put(hdr.bpp.cmd[4].a1p1Category, hdr.bpp.cmd[4].a1p1Value, hdr.bpp.cmd[4].a1p2Category, hdr.bpp.cmd[4].a1p2Value);

                        else if (hdr.bpp.cmd[4].a1Type == BPP_ACTION_DROP)
                            _drop();
                    }
                }
            }

            if (hdr.bpp.cmd.size > 5 && hdr.bpp.cmd[5].isValid()) {
                bit<1> c_result = 0;
                bit<1> c1_result = 0;
                bit<1> c2_result = 0;
                bit<16> conditionType = 1;

                if (hdr.bpp.cmd[5].conditionLength > 0) {
                    compute_condition(c1_result, hdr.bpp.cmd[5].c1Negation, hdr.bpp.cmd[5].c1Type, hdr.bpp.cmd[5].c1p1Category, hdr.bpp.cmd[5].c1p1Value, hdr.bpp.cmd[5].c1p2Category, hdr.bpp.cmd[5].c1p2Value);

                    if (hdr.bpp.cmd[5].conditionLength > 1) {
                        compute_condition(c2_result, hdr.bpp.cmd[5].c2Negation, hdr.bpp.cmd[5].c2Type, hdr.bpp.cmd[5].c2p1Category, hdr.bpp.cmd[5].c2p1Value, hdr.bpp.cmd[5].c2p2Category, hdr.bpp.cmd[5].c2p2Value);
                        conditionType = hdr.bpp.cmd[5].conditionType;
                    } else {
                        c2_result = 1;
                    }

                    compute_condition_set(c_result, conditionType, c1_result, c2_result);
                }

                if (hdr.bpp.cmd[5].conditionLength == 0 || c_result == 1) {
                    if (hdr.bpp.cmd[5].actionLength > 0) {
                        if (hdr.bpp.cmd[5].a1Type == BPP_ACTION_SUM && hdr.bpp.cmd[5].a1Length == 2)
                            sum(hdr.bpp.cmd[5].a1p1Category, hdr.bpp.cmd[5].a1p1Value, hdr.bpp.cmd[5].a1p2Category, hdr.bpp.cmd[5].a1p2Value);

                        else if (hdr.bpp.cmd[5].a1Type == BPP_ACTION_PUT && hdr.bpp.cmd[5].a1Length == 2)
                            sum(hdr.bpp.cmd[5].a1p1Category, hdr.bpp.cmd[5].a1p1Value, hdr.bpp.cmd[5].a1p2Category, hdr.bpp.cmd[5].a1p2Value);

                        else if (hdr.bpp.cmd[5].a1Type == BPP_ACTION_DROP)
                            _drop();
                    }
                }
            }

            if (hdr.bpp.cmd.size > 6 && hdr.bpp.cmd[6].isValid()) {
                bit<1> c_result = 0;
                bit<1> c1_result = 0;
                bit<1> c2_result = 0;
                bit<16> conditionType = 1;

                if (hdr.bpp.cmd[6].conditionLength > 0) {
                    compute_condition(c1_result, hdr.bpp.cmd[6].c1Negation, hdr.bpp.cmd[6].c1Type, hdr.bpp.cmd[6].c1p1Category, hdr.bpp.cmd[6].c1p1Value, hdr.bpp.cmd[6].c1p2Category, hdr.bpp.cmd[6].c1p2Value);

                    if (hdr.bpp.cmd[6].conditionLength > 1) {
                        compute_condition(c2_result, hdr.bpp.cmd[6].c2Negation, hdr.bpp.cmd[6].c2Type, hdr.bpp.cmd[6].c2p1Category, hdr.bpp.cmd[6].c2p1Value, hdr.bpp.cmd[6].c2p2Category, hdr.bpp.cmd[6].c2p2Value);
                        conditionType = hdr.bpp.cmd[6].conditionType;
                    } else {
                        c2_result = 1;
                    }

                    compute_condition_set(c_result, conditionType, c1_result, c2_result);
                }

                if (hdr.bpp.cmd[6].conditionLength == 0 || c_result == 1) {
                    if (hdr.bpp.cmd[6].actionLength > 0) {
                        if (hdr.bpp.cmd[6].a1Type == BPP_ACTION_SUM && hdr.bpp.cmd[6].a1Length == 2)
                            sum(hdr.bpp.cmd[6].a1p1Category, hdr.bpp.cmd[6].a1p1Value, hdr.bpp.cmd[6].a1p2Category, hdr.bpp.cmd[6].a1p2Value);

                        else if (hdr.bpp.cmd[6].a1Type == BPP_ACTION_PUT && hdr.bpp.cmd[6].a1Length == 2)
                            sum(hdr.bpp.cmd[6].a1p1Category, hdr.bpp.cmd[6].a1p1Value, hdr.bpp.cmd[6].a1p2Category, hdr.bpp.cmd[6].a1p2Value);

                        else if (hdr.bpp.cmd[6].a1Type == BPP_ACTION_DROP)
                            _drop();
                    }
                }
            }

            if (hdr.bpp.cmd.size > 7 && hdr.bpp.cmd[7].isValid()) {
                bit<1> c_result = 0;
                bit<1> c1_result = 0;
                bit<1> c2_result = 0;
                bit<16> conditionType = 1;

                if (hdr.bpp.cmd[7].conditionLength > 0) {
                    compute_condition(c1_result, hdr.bpp.cmd[7].c1Negation, hdr.bpp.cmd[7].c1Type, hdr.bpp.cmd[7].c1p1Category, hdr.bpp.cmd[7].c1p1Value, hdr.bpp.cmd[7].c1p2Category, hdr.bpp.cmd[7].c1p2Value);

                    if (hdr.bpp.cmd[7].conditionLength > 1) {
                        compute_condition(c2_result, hdr.bpp.cmd[7].c2Negation, hdr.bpp.cmd[7].c2Type, hdr.bpp.cmd[7].c2p1Category, hdr.bpp.cmd[7].c2p1Value, hdr.bpp.cmd[7].c2p2Category, hdr.bpp.cmd[7].c2p2Value);
                        conditionType = hdr.bpp.cmd[7].conditionType;
                    } else {
                        c2_result = 1;
                    }

                    compute_condition_set(c_result, conditionType, c1_result, c2_result);
                }

                if (hdr.bpp.cmd[7].conditionLength == 0 || c_result == 1) {
                    if (hdr.bpp.cmd[7].actionLength > 0) {
                        if (hdr.bpp.cmd[7].a1Type == BPP_ACTION_SUM && hdr.bpp.cmd[7].a1Length == 2)
                            sum(hdr.bpp.cmd[7].a1p1Category, hdr.bpp.cmd[7].a1p1Value, hdr.bpp.cmd[7].a1p2Category, hdr.bpp.cmd[7].a1p2Value);

                        else if (hdr.bpp.cmd[7].a1Type == BPP_ACTION_PUT && hdr.bpp.cmd[7].a1Length == 2)
                            put(hdr.bpp.cmd[7].a1p1Category, hdr.bpp.cmd[7].a1p1Value, hdr.bpp.cmd[7].a1p2Category, hdr.bpp.cmd[7].a1p2Value);

                        else if (hdr.bpp.cmd[7].a1Type == BPP_ACTION_DROP)
                            _drop();
                    }
                }
            }
        }
    }
}

#endif

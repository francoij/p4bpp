#ifndef _HEADERS_ICMP_P4_
#define _HEADERS_ICMP_P4_

/*
 *  ICMP HEADER
 */

header icmp_t {
    bit<8> type;
    bit<8> code;
    bit<16> checksum;
}

#endif
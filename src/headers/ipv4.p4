#ifndef _HEADERS_IPV4_P4_
#define _HEADERS_IPV4_P4_

/*
 *  IPV4 HEADER
 */

typedef bit<32> ipv4Addr_t;
typedef bit<8> ipv4Protocol_t;

const ipv4Protocol_t IPV4_ICMP_PROTOCOL = 0x01;
const ipv4Protocol_t IPV4_BPP_PROTOCOL = 0xfd;

header ipv4_t {
    bit<4>          version;
    bit<4>          ihl;
    bit<8>          tos;
    bit<16>         length;
    bit<16>         id;
    bit<3>          flags;
    bit<13>         fragOffset;
    bit<8>          ttl;
    ipv4Protocol_t  protocol;
    bit<16>         checksum;
    ipv4Addr_t      srcAddr;
    ipv4Addr_t      dstAddr;
}

#endif
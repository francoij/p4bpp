#ifndef _HEADERS_ETHERNET_P4_
#define _HEADERS_ETHERNET_P4_

/*
 *  ETHERNET HEADER
 */

typedef bit<48> macAddr_t;
typedef bit<16> etherType_t;

const etherType_t TYPE_IPV4 = 0x0800;
const etherType_t TYPE_IPV6 = 0x86dd;

header ethernet_t {
    macAddr_t   dstAddr;
    macAddr_t   srcAddr;
    etherType_t etherType;
}

#endif
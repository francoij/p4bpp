#ifndef _HEADERS_P4_
#define _HEADERS_P4_

/*
 *  HEADERS
 */

#include "ethernet.p4"
#include "ipv4.p4"
#include "ipv6.p4"
#include "bpp.p4"
#include "icmp.p4"

header_union ip_t {
    ipv4_t      ipv4;
    ipv6_t      ipv6;
}

struct headers {
    @name("ethernet")
    ethernet_t          ethernet;

    @name("ip")
    ip_t                ip;

    @name("bpp")
    bppBlock_t          bpp;

    @name("bpp_md")
    bppMetadata_t       bpp_md;

    @name("icmp")
    icmp_t              icmp;
}

#endif
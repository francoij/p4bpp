#ifndef _HEADERS_IPV6_P4_
#define _HEADERS_IPV6_P4_

/*
 *  IPV6 HEADER
 */

typedef bit<128> ipv6Addr_t;
typedef bit<8> ipv6Protocol_t;

const ipv6Protocol_t IPV6_ICMP_PROTOCOL = 0x3a;
const ipv6Protocol_t IPV6_BPP_HEADER = 0xfd;

header ipv6_t {
    bit<4>              version;
    bit<8>              trafficClass;
    bit<20>             flowLabel;
    bit<16>             length;
    ipv6Protocol_t      protocol;
    bit<8>              hopLimit;
    ipv6Addr_t          srcAddr;
    ipv6Addr_t          dstAddr;
}


#endif
#include <core.p4>
#include <v1model.p4>

#include "parser.p4"
#include "checksum.p4"
#include "traffic.p4"
#include "deparser.p4"

V1Switch(
    BPPParser(),
    BPPVerifyChecksum(),
    BPPIngress(),
    BPPEgress(),
    BPPComputeChecksum(),
    BPPDeparser()
) main;
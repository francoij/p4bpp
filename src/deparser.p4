#ifndef _DEPARSER_P4_
#define _DEPARSER_P4_

#include "headers/headers.p4"

control BPPDeparser(packet_out packet, in headers hdr) {
    apply {
        packet.emit(hdr.ethernet);
        packet.emit(hdr.ip.ipv4);
        packet.emit(hdr.ip.ipv6);
        packet.emit(hdr.bpp.hdr);
        packet.emit(hdr.bpp.cmd);
        packet.emit(hdr.bpp_md);
    }
}

#endif